# @uiii-lib tsconfig

Typescript config unify specific behavior across projects. Do not use standalone, use it as an extension to an existing project with working tsconfig.

## Usage

Register `@uiii-lib` package registry to npm

```
npm config set @uiii-lib:registry https://gitlab.com/api/v4/packages/npm/
```

Install

```
yarn add --dev @uiii-lib/tsconfig
```

Rename the current `tsconfig.json` to e.g. `tsconfig.base.json`.

Put into new `tsconfig.json`

```json
{
  "extends": [
	"./tsconfig.base.json",
	"@uiii-lib/tsconfig/<env>.json"
  ]
}
```

Where `<env>` can be either `node` or `browser`.
